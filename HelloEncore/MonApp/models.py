from django.db import models

# Create your models here.

class Ordinateur(models.Model):
    numero_erie = models.IntegerField(default=0)
    marque = models.CharField(max_length=100)

class Piece(models.Model):
    ordinateur = models.ForeignKey(Ordinateur, on_delete=models.CASCADE)
    cout = models.DecimalField(max_digits=7, decimal_places=2)
